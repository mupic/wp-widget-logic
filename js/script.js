jQuery(document).ready(function($) {
	new_select = $('.widget-options .wrap-select.new-select');
	if (!new_select.length) return;

	var $new_select_innerHTML = new_select[0]['innerHTML'],
		$hide_new_select_innerHTML = '';
	$('select[name*="hide_widget_logic"]:not(.not-mine-select)', new_select).each(function() {
		select = $(this).closest('.wrap-select.new-select');
		$hide_new_select_innerHTML = select[0]['innerHTML'];
	});

	$.fn.add_disabled_select = function() {
		var $this = this;
		$('option', $this).removeAttr('disabled');
		$('select:not(.not-mine-select)', $this).each(function() {
			var $this2 = $(this);
			if (!!$this2.val())
				$('option[value="' + $this2.val() + '"]', $this).each(function() {
					$this3 = $(this);
					if ($this2.val() != $this3.parent('select').val())
						$this3.attr('disabled', 'disabled')
				});
		});
	}

	$.fn.max_number_select = function() {
		var $this = this,
			max = 0;
		$this.attr('number', function(index, value) {
			max = (max < (+value)) ? (+value) : max;
		});

		return max;
	}

	$('.widget-options').each(function() {
		$this = $(this);
		$count = $('.wrap-select', $this).max_number_select();
		$('.wrap-select.new-select:not(.hide_widget_logic)', $this).attr('number', $count + 1).removeClass('new-select load-ajax').html($new_select_innerHTML.replace(/{i}/gi, $count + 1));
		$('.wrap-select.new-select.hide_widget_logic', $this).attr('number', $count + 1).removeClass('new-select load-ajax').html($hide_new_select_innerHTML.replace(/{i}/gi, $count + 1));

		$this.add_disabled_select();
	});

	$(document).on('click', '.add-wrap-select', function() {
		$this = $(this);
		options = $this.closest('.widget-options');
		$count = $('.wrap-select', options).max_number_select();

		if ($this.closest('.wrap-select').hasClass('hide_widget_logic')) {
			$('<div number="' + ($count + 1) + '" class="wrap-select">').prependTo(options).html($hide_new_select_innerHTML.replace(/{i}/gi, $count + 1));
		} else {
			$('<div number="' + ($count + 1) + '" class="wrap-select">').prependTo(options).html($new_select_innerHTML.replace(/{i}/gi, $count + 1));
		}

		options.add_disabled_select();
	});
	$(document).on('click', '.remove-wrap-select', function() {
		$this = $(this);
		options = $this.closest('.widget-options');
		select = $this.closest('.wrap-select');
		if ($('.wrap-select', options).length > 1) {
			select.remove();
		} else {
			$('input, select', select).val('').removeAttr('checked').removeAttr('selected');
			$('[callback]', select).hide();
		}

		options.add_disabled_select();
	});

	$(document).on('change', '.select-callback', function() {
		$this = $(this);
		options = $this.closest('.widget-options');
		select = $this.closest('.wrap-select');
		value = $this.val();

		//если произошло сохранение виджетов, то нужно переопределить переменные для полей формы (заменить {i} на цифру)
		if (select.hasClass('load-ajax')) {
			$count = $('.wrap-select', options).max_number_select();

			if (select.hasClass('hide_widget_logic')) {
				select.attr('number', $count + 1).html($hide_new_select_innerHTML.replace(/{i}/gi, $count + 1))
			} else {
				select.attr('number', $count + 1).html($new_select_innerHTML.replace(/{i}/gi, $count + 1))
			}

			$('[value="' + value + '"]', select)[0].selected = true;

			select.removeClass('load-ajax');
		}

		$('[callback]', select).hide();
		$('[callback="' + value + '"]', select).show();

		options.add_disabled_select();
	});

});