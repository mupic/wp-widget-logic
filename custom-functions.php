<?php

namespace WidgetLogic;

if(!defined('ABSPATH')) exit;

class Custom_Functions{

	function is_administrator(){
		return current_user_can('administrator');
	}

}