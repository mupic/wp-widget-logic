<?php

namespace WidgetLogic;

if(!defined('ABSPATH')) exit;

if(!defined('WLG_URL'))
	define('WLG_URL', get_stylesheet_directory_uri().'/vendor/mupic/wp-widget-logic/'); //url

class WidgetLogic {
	public $options;
	private $callback;
	private $key_options;
	public $name_logic = 'widget_logic';
	public $name_hide_logic = 'hide_widget_logic';
	
	public function action(){
		$this->option();
		
		add_action('admin_enqueue_scripts', array($this, 'add_style_or_script')); //Подключаем скрипты
		add_filter('widget_update_callback', array($this, 'update')); //обновляем значения виджетов
		add_action('sidebar_admin_setup', array($this, 'action_widget')); //Добавляем поля к виджетам
		add_filter('sidebars_widgets', array($this, 'filter'), 10); // Фильтруем вывод виджетов
	}
	
	public function option(){

		include('custom-functions.php');

		$custom_functions = new Custom_Functions();

		/*В переменной callback не должно быть пробелов*/
		$this->options = array(
			array(
				'callback' => array('key' => 'is_administrator', 'function' => array($custom_functions, 'is_administrator')), //current_user_can('administrator')
				'name' => 'Администратор',
				'value' => '', //Если true то в функцию будут отправлены данные введенные в дополнительное тектсовое поле
				'desc' =>'Функция проверяет администратор ли просматривает страницу.',
				'main' => true, //Приоритет функций. Главная функция которая решает будет ли виджет отображаться
			),
			array(
				'callback' => 'is_user_logged_in',
				'name' => 'Авторизован',
				'value' => '',
				'desc' =>'Функция проверяет авторизован ли пользователь (вошел ли пользователь под своим логином).',
				'main' => true, //Приоритет функций. Главная функция которая решает будет ли виджет отображаться
			),
			array(
				'callback' => 'is_front_page',
				'name' => 'Главная страница',
				'value' => '',
				'desc' =>'Проверяет отображается ли главная страница сайта. Тег сработает в любом случае: будь это страница с выводом последних постов или страница где для главной страницы сайта установлена статическая страница.',
			),
			array(
				'callback' => 'is_home',
				'name' => 'Страница с последними постами',
				'value' => '',
				'desc' =>'Проверяет показывается ли страница с последними постами, обычно это главная страница сайта.',
			),
			array(
				'callback' => 'is_search',
				'name' => 'Поиск',
				'value' => '',
				'desc' =>'Срабатывает, когда отображается страница результатов поиска.',
			),
			array(
				'callback' => 'is_category',
				'name' => 'Категрия',
				'value' => true,
				'desc' =>'Проверяет показывается ли страница категорий или нет. Можно указать ID рубрики, название, ярлык рубрики (Приемер того какие можно указать значения: 9, blue-cheese, Stinky Cheeses)',
			),
			array(
				'callback' => 'is_page',
				'name' => 'Страница',
				'value' => true,
				'desc' =>'Проверяет отображается ли страница "постоянной страницы". Можно указать ID рубрики, название, ярлык (Приемер того какие можно указать значения: 42, o-saite, О сайте)',
			),
			array(
				'callback' => 'is_paged',
				'name' => 'Пагинация',
				'value' => '',
				'desc' =>'Проверяет отображается ли страница пагинации (страница типа /page/2, /page/3).',
			),
			array(
				'callback' => 'is_tag',
				'name' => 'Метка',
				'value' => true,
				'desc' =>'Проверят отображается ли страница архива по меткам. Можно указать ID метки, название, ярлык метки (Приемер того какие можно указать значения: 9, blue-cheese, Stinky Cheeses)',
			),
			array(
				'callback' => 'is_tax',
				'name' => 'Таксономия',
				'add_select' => array(
					'taxonomy' => array('multiple' => true, 'key_name'=>array('labels', 'menu_name'), 'key_value'=>'name', 'array'=> get_taxonomies( array('public'=> true, '_builtin' => false), 'objects', 'and' ) ), //key_name, key_value - путь до значения
					'position_var' => array('taxonomy'=>'', 'value'=>''), //Расположение аргументов для вызова фукции
				),
				'value' => true,
				'desc' =>'Срабатывает на архивной странице пользовательской таксономии. Нужно указать название типа поста.',
			),
			array(
				'callback' => 'is_post_type_archive',
				'name' => 'Архивная страница записей произвольного типа',
				'value' => true,
				'desc' =>'Проверяет находится ли пользователь на архивной странице записей произвольного типа. Страницей архива произвольного типа считается страница, на которой выводятся записи нового типа (/?post_type=custom). Если, например, новый тип записей называется "book", то такая страница будет иметь УРЛ: http://site.ru/book/ (это обычно, зависит от настроек ЧПУ).',
			),
			array(
				'callback' => 'is_singular',
				'name' => 'Любая страница',
				'value' => true,
				'desc' =>'Срабатывает, когда пользователь находится на любых отдельных типах страниц: пост, постоянная страница, вложение или произвольный тип записи. Можно указать конкретный тип записи, который нужно проверить (Приемер того какие можно указать значения: post, page).',
			),
			array(
				'callback' => 'is_archive',
				'name' => 'Любой архив',
				'value' => '',
				'desc' =>'Срабатывает, когда показывается любая из архивных страниц (страница: категории, метки, автора, даты).',
			),
			array(
				'callback' => 'is_404',
				'name' => '404',
				'value' => '',
				'desc' =>'Проверяет показывается ли страница ошибки 404 (HTTP 404: страница не найдена).',
			),
			array(
				'callback' => 'wp_is_mobile',
				'name' => 'Мобильное устройство',
				'value' => '',
				'desc' =>'Проверяет не с мобильного ли устройства зашел пользователь.',
			)
		);
		
		foreach($this->options as $options){
			if(is_array($options['callback'])){
				$key = $options['callback']['key'];
			}else{
				$key = $options['callback'];
			}
			$this->callback[$key] = $options['callback'];
			$this->key_options[$key] = $options;
		}
	}
	
	private function get_key_callback($array){
		return is_array($array)? $array['key'] : $array;
	}
	private function get_callback($array){
		return is_array($array)? $array['function'] : $array;
	}

	public function filter($sidebars_widgets){
		if(!is_404() && is_admin()) return $sidebars_widgets;

		if($sidebars_widgets)
			foreach($sidebars_widgets as $widget_area => $widget_list){
				if ($widget_area == 'wp_inactive_widgets' || empty($widget_list)) continue;
				foreach($widget_list as $key => $widget_id){
					$bool = array();
					$main = array();
					$id = explode('-', $widget_id);
					$get_option = get_option('widget_'.$id[0]);
					$get_option = $get_option[$id[1]];
					if(empty($get_option['widget_logic']) && empty($get_option['hide_widget_logic'])) continue;
					
					foreach(array($this->name_logic, $this->name_hide_logic) as $logic){
						$inverse = ($this->name_hide_logic == $logic)? true : false;
						if(empty($sidebars_widgets[$widget_area][$key])) break;
						
						if($get_option[$logic])
							foreach($get_option[$logic] as $key_callback => $value){
								if(!isset($this->key_options[$key_callback])) continue;
								$callback = $this->get_callback($this->key_options[$key_callback]['callback']);
								if(!empty($value)){
									if(isset($value['value'])) $value['value'] = explode(',', strtr($value['value'], array("\s"=>"")));
									if(count($value) > 1){
										foreach($this->options as $options){
											if($this->get_key_callback($options['callback']) == $key_callback){
												$position_var = isset($options['add_select']['position_var'])? $options['add_select']['position_var'] : array();
												$value = array_merge($position_var, $value);
												break;
											}
										}
									}
								}else{
									$value = array();
								}
								
								if($inverse){ //Если мы находимся там где не должен отображаться виджет
									if(call_user_func_array($callback, $value))
										unset($sidebars_widgets[$widget_area][$key]);
								}else{
									if(isset($this->key_options[$key_callback]['main']) && $this->key_options[$key_callback]['main']){ //если главная функция, то мы удалим виджет если false
										$main[] = call_user_func_array($callback, $value);
									}else{
										$bool[] = call_user_func_array($callback, $value);
									}
								}
							}
					}
					
					if(!empty($sidebars_widgets[$widget_area][$key])){
						if(!empty($main) && in_array(false, $main)){
							unset($sidebars_widgets[$widget_area][$key]);
						}elseif(!empty($bool) && !in_array(true, $bool)){
							unset($sidebars_widgets[$widget_area][$key]);
						}
					}

				}
			}
		return $sidebars_widgets;
	}
	
	public function action_widget(){
		global $wp_registered_widget_controls;
		foreach ($wp_registered_widget_controls as $id => $widget) {
			$controls = &$wp_registered_widget_controls[$id];
			$controls['params'][] = $id;
			if (isset($controls['callback'])) {
				$controls['callback_redirect'] = $controls['callback'];
			}
			$controls['callback'] = array($this, 'form');
		}
	}
	
	public function form(){
		global $wp_registered_widget_controls, $get_option;
		$params = func_get_args();
		$widget_id = $params[count($params) - 1];
		$widget_controls = $wp_registered_widget_controls[$widget_id];
		
		if($params[0]['number'] > 0){
			$get_option = get_option('widget_'.$widget_controls['id_base']);
			$get_option = $get_option[$widget_controls['params'][0]['number']];
		}

		if(isset($widget_controls['callback_redirect'])){
			$callback = $widget_controls['callback_redirect'];
			if (is_callable($callback)) {
				call_user_func_array($callback, $params);
			}
		} ?>
			
			<h3>Настройки</h3>
			<h4>Показывать если:</h4>
			<div class="widget-options">
				<?php $this->select($this->name_logic); ?>
			</div>
			<h4>Не показывать если:</h4>
			<div class="widget-options">
				<?php $this->select($this->name_hide_logic); ?>
			</div>
			
	<?php }
	
	public function update($instance){
		if(!empty($_POST[$this->name_logic]))
			$instance[$this->name_logic] = $this->unset_clear_var($_POST[$this->name_logic]);
		if(!empty($_POST[$this->name_hide_logic]))
			$instance[$this->name_hide_logic] = $this->unset_clear_var($_POST[$this->name_hide_logic]);
		return $instance;
	}
	
	private function unset_clear_var($array = array()){
		$instance = array();
		if(!empty($array))
			foreach($array as $arr){
				if(empty($arr['callback']) || !in_array($arr['callback'], array_keys($this->callback)))
					continue;
				if(empty($arr[$arr['callback']]['value']))
					unset($arr[$arr['callback']]['value']);
				$instance[$arr['callback']] = !empty($arr[$arr['callback']])? $arr[$arr['callback']] : '';
			}

		return $instance;
	}
	
	private function select($name){
		global $get_option;
		if(!empty($get_option[$name])){
			$get_options = array_merge(array('none'=>''), $get_option[$name]);
		}else{
			$get_options = array('none'=>'');
		}
		
		$i=1;
		foreach($get_options as $cb => $go){
			$input = '';
			$add_select = '';
			$main_select = '';
			$desc = '';
			
			if($cb == 'none'){
				$class = 'new-select load-ajax';
				$save_i = $i;
				$i = '{i}';
			}else{
				$class = '';
				$i = $save_i? $save_i : $i;
				$save_i = '';
			}

			echo '<div number="'.$i.'" class="wrap-select '.$class.' '.$name.'">';
			$main_select .= '<select class="select-callback" name="'.$name.'['.$i.'][callback]"><option value="">-----</option>';
				foreach($this->options as $option){
					$key_callback = $this->get_key_callback($option['callback']);
					$dn = 'style="display:none;"';
					$value = '';
					
					if($key_callback == $cb){
						$value = !empty($go['value'])? $go['value'] : '';
						$dn = '';
					}
					
					$main_select .= '<option value="'.$key_callback.'" '.selected($key_callback, $cb, 0).'>'.$option['name'].' ('.$key_callback.')</option>';

					$desc .= '<span class="select-description" '.$dn.' callback="'.$key_callback.'">[?]<span>'.$option['desc'].'</span></span>'; //Описание
					
					if($option['value'])
						$input .= '<input title="Через запятую если несколько" type="text" '.$dn.' callback="'.$key_callback.'" name="'.$name.'['.$i.']['.$key_callback.'][value]" value="'.$value.'">';

					if( !empty($option['add_select']) && is_array($option['add_select']) ){

						foreach($option['add_select'] as $key => $select){
							if('position_var' == $key) continue;
							$multiple = $select['multiple']? 'multiple':'';
							$add_select .= '<select class="not-mine-select" '.$dn.' '.$multiple.' callback="'.$key_callback.'" name="'.$name.'['.$i.']['.$key_callback.']['.$key.'][]">';
								foreach($select['array'] as $array){
									$svalue = $this->get_level_array($array, $select['key_value']);
									$sname = $this->get_level_array($array, $select['key_name']);
									$add_select .= '<option value="'.$svalue.'" '.selected(!empty($go[$key]) && in_array($svalue, $go[$key]), true, 0).'>'.$sname.'</option>';
								}
							$add_select .= '</select>';
						}

					}

				}
			$main_select .= '</select><div class="remove-wrap-select">-</div><div class="add-wrap-select">+</div>';

			echo $desc;
			echo $main_select;
			echo $input;
			echo $add_select;

			echo '</div>';
			
			$i++;
		}
		
	}
	
	public function get_level_array($array, $keys){
		$array = (array) $array;
		if(is_array($keys)){
			
			foreach($keys as $key){
				if(isset($duble_array)){
					$duble_array = is_array($duble_array[$key])? (array) $duble_array[$key] : $duble_array[$key];
				}else{
					$duble_array = (is_array($array[$key]) || is_object($array[$key]))? (array) $array[$key] : $array[$key];
				}
			}
			
		}else{
			$duble_array = $array[$keys];
		}
		
		return $duble_array;
	}
	
	public function add_style_or_script(){
		wp_enqueue_style( 'widget_logic', WLG_URL.'css/style.css' );
		wp_enqueue_script( 'widget_logic', WLG_URL.'js/script.js', array('jquery') );
	}
	
}

add_action('init', array(new WidgetLogic, 'action'));